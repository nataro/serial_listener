import logging
import re

import serial
from tornado.ioloop import IOLoop

# from util.measure.pico2000_capture import Ps2000Block
from util.measure.cam_capture import CaptureDevice
from util.measure.pico5000_capture import Ps5000Block
from util.measure_manager import RecordManagerDirector, PicoManagerBuilder, CaptureManagerBuilder

logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')


class SerialPolling(object):
    """
    polling_hzで指定した周期で、シリアルポートの読み込みを行い、delimiterで区切られた単位で
    文字列を切り取る。
    （残りの文字列はself._read_bufferに蓄えられて、pollingの際に切り取られるのを待つことになる）
    切り取られた文字列はbytes型としてcallback()に渡される。
    callbackにおいて、受け取ったbytesをserialized_dictで解析し、dict型に変換する。
    さらに、解析して得たdict型を***に渡して、メインとなる処理を実行する。

    このクラス自体をwithで管理できるように、このクラスに__enter__と__exit__を実装する
    このクラスの__enter__にDataRecordManager経由でCallableMeasurementをopenする処理を書いて、
    このクラスの__exit__にDataRecordManager経由でCallableMeasurementをcloseする処理を書けばよいのでは？
    """

    def __init__(self,
                 port: str,
                 baudrate: int,
                 delimiter: bytes = b'\n',
                 polling_hz: float = 1000,
                 pico: RecordManagerDirector = None,
                 cap: RecordManagerDirector = None,
                 ):
        """
        Parameters
        ----------
        port : str

        baudrate : int

        delimiter : bytes
            シリアルポートから読み込んだbyte配列の境界文字として用いる
        polling_hz : float
            ポーリングの実行周期[hz]
        """
        self._serial_port = serial.Serial(port, baudrate)
        self._delimiter = delimiter
        self._io_loop = IOLoop.current()
        self._read_buffer = b''  # シリアルポートから読み込んだbyte配列を格納
        self._loop_freq = polling_hz
        '''正規表現のマッチングに用いるオブジェクト'''
        self._slash_pattern = re.compile(b'/')  # '/'にマッチする正規表現
        self._minus_space_pattern = re.compile(b'-\s+')  # '-'+'複数の半角スペース'にマッチする正規表現
        self._space_pattern = re.compile(b'\s+')  # '複数の半角スペース'にマッチする正規表現

        # # データロギングを管理するクラス
        # self.data_recorder = DataRecordManager(audio_name='1-2 (QUAD-CAPTURE)')

        # データロギングを管理するクラス
        self._pico_director = pico
        self._cap_director = cap

        self.loop()  # 定義したループ処理を実行

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        self._pico_director.device_open()
        # カメラデバイスのopen,closeは使用時に自動で行うので、ここでopenしなくてもよい
        self._cap_director.device_open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        self._pico_director.device_close()
        # カメラデバイスのopen,closeは使用時に自動で行うので、ここでcloseしなくてもよい
        self._cap_director.device_close()

    def loop(self):
        """再帰的に自分自身を呼び出してループ処理"""
        # self.ioloop.add_timeout(time.time() + 1 / self.LOOP_FREQ, self.loop)
        self._io_loop.call_later(1 / self._loop_freq, self.loop)  # こっちでもよさそう
        self.read()

    def read(self):
        """シリアルポートの読み込み"""
        self._read_buffer += self._serial_port.read(self._serial_port.inWaiting())
        # 以下条件確認．任意の条件に変更可能
        for segment in self._read_buffer.split(self._delimiter)[:-1]:
            self.callback(segment)  # 切り取ったbytesを渡してメインの処理を呼び出す
        # delimiterで切り取られた文字の残りの文字列をバッファに戻しておく
        # （処理が遅れるとどんどん溜まる？）
        self._read_buffer = self._read_buffer.split(self._delimiter)[-1]

    def callback(self, receive_segment: bytes):
        """
        Parameters
        ----------
        receive_segment : bytes

        """
        dic = self.serialized_dict(receive_segment)
        # print('Received:', dic)
        # ここで、dictを引数として受けてその内容を基に処理を行うメインの関数を実行する
        self._pico_director(dic)
        self._cap_director(dic)

    def serializer(self, byte_arr: bytes) -> tuple:
        """
        Parameters
        ----------
        byte_arr : bytes
            e.g. byte_arr = b'ZP-    .200'
            byte_arrの文字の直後の「-」が付いている場合は、valueに相当する数値の符号をマイナスにする
        Returns
        -------
        tuple
            (str_param, float_value)  or (None, None)
        """
        is_minus = True
        # 「b'-    '」というパターンでスプリットするか、「b'   '」というパターンでスプリットするか
        spl_list = self._minus_space_pattern.split(byte_arr)
        if len(spl_list) < 2:
            is_minus = False
            spl_list = self._space_pattern.split(byte_arr)
        try:
            key = spl_list[0].decode()
            value = float(spl_list[1])
            if is_minus:
                value = -value
            return key, value
        except (IndexError, ValueError) as e:
            logging.error(e)
            # print(e)
            return None, None

    def serialized_dict(self, byte_arr: bytes) -> dict:
        """
        MC側で、「DPRNT[ST#1[53]/ZP#2[53]]」という感じで、「/」という文字を含めて出力されたバイトアレイを想定
        スラッシュで区切られたバイトアレイを切り分けてシリアライズして、辞書型で結果を返す

        Parameters
        ----------
        byte_arr : bytes
            e.g. byte_arr = b'ST    5.000/ZP-    .080'
        Returns
        -------
        dict
            {str_param: float_value, ... }  or {}
        """
        dic = {}
        spl_list = self._slash_pattern.split(byte_arr)
        for arr in spl_list:
            tup = self.serializer(arr)
            if tup[0]:  # keyとvalueに相当するタプルがある場合
                # タプルを展開して、keyの重複を許可せずに追加
                dic.setdefault(*tup)
        return dic


if __name__ == "__main__":
    # serial_polling = SerialPolling("COM3", 115200)  # test
    # serial_polling = SerialPolling("COM3", 9600)  # machine
    # serial_polling = SerialPolling("COM1", 115200)  # test
    # serial_polling = SerialPolling("COM5", 115200)  # machine
    #
    # # windows の場合は(Ctrl+PauseもしくはCtrl+C)で止まる
    # try:
    #     IOLoop.instance().start()
    # except KeyboardInterrupt:
    #     IOLoop.instance().stop()

    # threshold = 2000  # [mv]
    threshold = 10000  # [mv]
    # samples = 2 ** 19  # 2^19=524,288
    # samples = 2 ** 24  # 2^24=16,777,216
    samples = 2 ** 20  # 2^20=1,048,576
    # samples = 2 ** 15  # 2^15=32,768
    auto_trg_ms = 200
    range_mode = 'PS5000A_10V'
    # range_mode = 'PS5000A_2V'

    pico_dev = Ps5000Block(
        th_mv=threshold,
        num_samples=samples,
        auto_trigger_time_ms=auto_trg_ms,
        range_mode=range_mode,
    )
    pm = PicoManagerBuilder(pico_dev)
    pd = RecordManagerDirector(pm)

    cap_dev = CaptureDevice(cam_num=0)
    cm = CaptureManagerBuilder(cap_dev)
    cd = RecordManagerDirector(cm)

    # with SerialPolling("COM5", 115200, manager=dm) as sp:
    # with SerialPolling("COM3", 9600, manager=dm) as sp:
    with SerialPolling("COM3", 19200, pico=pd, cap=cd) as sp:
    # with SerialPolling("COM5", 19200, pico=pd, cap=cd) as sp:
        # windows の場合は(Ctrl+PauseもしくはCtrl+C)で止まる
        try:
            IOLoop.instance().start()
        except KeyboardInterrupt:
            IOLoop.instance().stop()
