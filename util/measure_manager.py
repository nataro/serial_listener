import logging
import threading
from abc import ABCMeta, abstractmethod
from datetime import datetime
from pathlib import Path
from typing import Sequence, Optional, Dict

import cv2

# from .path_tools import FileSave
from .measure.measure import CallableMeasurement, PicoData, PicoDataH5FileSave

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class RecordManagerBuilder(metaclass=ABCMeta):

    def __init__(self, device: CallableMeasurement):
        self._device = device
        self._event = threading.Event()  # 計測処理に関するスレッドを同期して開始するために使用
        self._lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def recording_thread_manage(self,
                                # event: threading.Event,
                                # lock: threading.Lock,
                                # device: CallableMeasurement,
                                dir_path: str,
                                file_name: str,
                                received: dict) -> None:
        """
        計測用のスレッドを管理する

        Parameters
        ----------
        event : threading.Event
        lock : threading.Lock
        # device : CallableMeasurement
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        """
        record_t = threading.Thread(
            name=self.__class__.__name__ + '-record_thread',
            target=self.record_via_device,
            # args=(event, lock, device, dir_path, file_name, received)
            args=(self._event, self._lock, dir_path, file_name, received)
        )
        record_t.start()
        # 複数種類のスレッドを同時に実行させたいことがあるかもしれないので、event.set()でタイミングをあわせる予定
        # event.set()  # event.wait()で待機しているプロセスを全部同時に解除
        self._event.set()  # event.wait()で待機しているプロセスを全部同時に解除

    @abstractmethod
    def record_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          # device: CallableMeasurement,
                          dir_path: str,
                          file_name: str,
                          received: dict,
                          ext: str):
        pass

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)


class CaptureManagerBuilder(RecordManagerBuilder):
    """
    usbカメラ等を用いて画像を撮影する
    """

    def record_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          # device: CallableMeasurement,
                          dir_path: str,
                          file_name: str = '',
                          received: dict = None,
                          ext: str = 'jpg'):
        """
        Parameters
        ----------
        event : threading.Event
        lock : threading.Lock
        device : CallableMeasurement
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        ext : str
        """
        logging.debug(self.__class__.__name__ + ' : record thread start')
        event.wait()  # event.set()が実行されるまで待機
        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')
            dt = datetime.now()
            file_path = dir_path + "/{0}.".format(dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext

            data = self._device()
            if data is not None:
                cv2.imwrite(file_path, data)

        logging.debug(self.__class__.__name__ + ' : unlock')
        # logging.debug(self.__class__.__name__ + ' : record end')

    def check_dict(self, received: Dict) -> Optional[str]:
        """
        received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
        というような辞書型を受け取って、データを保存するかどうか判断する
        データを保存する場合はself.recording()を呼び出す

        Parameters
        ----------
        received : dict
            e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}

        """
        must_keys = ('CC',)
        is_involve = self.keys_check(received.keys(), must_keys)
        if not is_involve:
            return None

        counts = received.get('CC')  # 穴加工数を'CC'というkeyで受け取る予定
        try:
            counts = int(counts)
        except (ValueError, TypeError) as e:
            logging.error(e)
            # print('loggin check value error')
        else:
            file_name = "cc{0}".format(counts)
            return file_name
        return None


class PicoManagerBuilder(RecordManagerBuilder):
    """
    PicoScopeで計測データをHDF5形式で保存する
    """

    def __init__(self, device: CallableMeasurement, ):
        super().__init__(device)
        self._record_count_interval = 10  # 何count毎に録音するか
        # self._record_steps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]  # 何step目を録音対象とするか
        self._record_steps = [2, 4, 6, 8, 10, 12, 14,]  # 何step目を録音対象とするか
        self._file_save_tool = PicoDataH5FileSave()  # ファイル保存用の自作ツール(拡張子は.hdf5もしくは.h5にすること）

    def record_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          dir_path: str,
                          file_name: str = '',
                          received: dict = None,
                          ext: str = 'hdf5'):
        """
        Parameters
        ----------
        event : threading.Event
        lock : threading.Lock
        device : CallableMeasurement
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        ext : str
        """
        logging.debug(self.__class__.__name__ + ' : record thread start')
        event.wait()  # event.set()が実行されるまで待機
        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')
            dt = datetime.now()
            file_path = dir_path + "/{0}.".format(dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext

            # データの収集(PicoData型)
            data = self._device()
            # データ(PicoData型)にマシニングから受け取った情報を追加する
            data.machining_condition = received
            # 取得したデータがオーバーフローしていないかなどを確認
            self.pico_data_errror_check(data, file_path)
            # データの保存
            self._file_save_tool(data, file_path)

            # print('pm:' + file_name)

        logging.debug(self.__class__.__name__ + ' : unlock')
        # logging.debug(self.__class__.__name__ + ' : record end')

    def check_dict(self, received: Dict) -> Optional[str]:
        """
        received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
        というような辞書型を受け取って、データを保存するかどうか判断する
        データを保存する場合はself.recording()を呼び出す

        Parameters
        ----------
        received : dict
            e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}

        """
        must_keys = ('AC', 'ST', 'ZP',)
        is_involve = self.keys_check(received.keys(), must_keys)
        if not is_involve:
            return None

        counts = received.get('AC')  # 穴加工数を'AC'というkeyで受け取る予定
        steps = received.get('ST')  # ステップ数を'ST'というkeyで受け取る予定
        depth = received.get('ZP')  # Z座標を'ZP'というkeyで受け取る予定
        try:
            counts = int(counts)
        except (ValueError, TypeError) as e:
            logging.error(e)
            # print('loggin check value error')
        else:
            if counts % self._record_count_interval == 0:
                if steps in self._record_steps:
                    file_name = "ac{0}_st{1}_zp{2:.3f}".format(counts, steps, depth)
                    return file_name
        return None

    @staticmethod
    def pico_data_errror_check(picodata: PicoData, info: str) -> None:
        """
        Parameters
        ----------
        picodata : PicoData
        info : str
        """
        assigned_dict = picodata.assigned_values
        if not assigned_dict:
            return

        over_flow = assigned_dict.get('over_flow')
        trg_auto_ms = assigned_dict.get('trigger_auto_ms')
        run_time_ms = assigned_dict.get('run_block_time_ms')
        run_elp_time_sec = assigned_dict.get('run_block_elapsed_time_sec')

        if over_flow:
            logger.error('over flow at :' + info)


class RecordManagerDirector(object):
    def __init__(self, builder: RecordManagerBuilder, dir_path: str = None):
        self._builder = builder
        self._storage_path = self.create_storage_directory(dir_path)

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        (received_dict,) = args
        # if not any(received_dict):
        if any(received_dict):  # dictが空かどうかのチェック
            # print(received_dict)
            logger.debug(self.__class__.__name__ + ': __call__() received : ', received_dict)
            file_name = self._builder.check_dict(received_dict)
            if file_name is not None:
                self.recording(file_name, received_dict)

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()

    def recording(self, file_name: str = '', received: dict = None) -> None:
        self._builder.recording_thread_manage(self._storage_path, file_name, received)

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())


# class DataRecordManager(object):
#     """
#     dict型で、count, step, depthに関する情報が渡されるとする。
#     例えば、「10coutの90step目で10.5depth（10穴目の90ステップ目、深さ10.5mm）」
#     """
#
#     def __init__(self, device: CallableMeasurement = None, dir_path: str = None):
#         """
#         Parameters
#         ----------
#         device : CallableMeasurement
#             (CallableMeasurementは、__call__()とopen()、close()というメソッドを実装した計測用のクラス)
#         """
#         self._record_device = device
#
#         self._record_count_interval = 10  # 何count毎に録音するか
#         self._record_steps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]  # 何step目を録音対象とするか
#
#         self._record_event = threading.Event()  # 計測処理に関するスレッドを同期して開始するために使用
#         self._record_lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする
#
#         self._storage_path = self.create_storage_directory(dir_path)
#         self._file_save_tool = PicoDataH5FileSave()  # ファイル保存用の自作ツール(拡張子は.hdf5もしくは.h5にすること）
#
#     def __call__(self, *args, **kwargs):
#         """
#         __call__()でメインの動作ができるようにしておく
#         """
#         (received_dict,) = args
#         # received_dict = args[0]
#         logger.debug('__call__() received : ', received_dict)
#
#         self.check_dict(received_dict)
#
#     def device_open(self):
#         self._record_device.open()
#         # pass
#
#     def device_close(self):
#         self._record_device.close()
#         # pass
#
#     def check_dict(self, received: dict):
#         """
#         これがメインとなる関数。
#         received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
#         というような辞書型を受け取って、データを保存するかどうか判断する
#         データを保存する場合はself.recording_thread_manage()を呼び出す
#
#         Parameters
#         ----------
#         received : dict
#             e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
#
#         """
#         counts = received.get('AC')  # 穴加工数を'AC'というkeyで受け取る予定
#         steps = received.get('ST')  # ステップ数を'ST'というkeyで受け取る予定
#         depth = received.get('ZP')  # Z座標を'ZP'というkeyで受け取る予定
#         try:
#             counts = int(counts)
#             steps = int(steps)
#             depth = float(depth)
#         except (ValueError, TypeError) as e:
#             logging.error(e)
#             # print('loggin check value error')
#         else:
#             if counts % self._record_count_interval == 0:
#                 if steps in self._record_steps:
#                     file_name = "ac{0}_st{1}_zp{2:.3f}".format(counts, steps, depth)
#                     # self.wave_record(file_name)
#                     # print(file_name)
#                     self.recording_thread_manage(self._record_event, self._record_lock, file_name, received)
#
#     def recording_thread_manage(self, event: threading.Event, lock: threading.Lock,
#                                 file_name: str = '',
#                                 received: dict = None):
#         """
#         Parameters
#         ----------
#         event : threading.Event
#         lock : threading.Lock
#         file_name : str
#         received : dict
#             マシニングセンタからシリアル通信で受け取った情報
#         """
#         record_t = threading.Thread(
#             name='record_thread',
#             target=self.record_via_device,
#             args=(event, lock, file_name, received)
#         )
#         record_t.start()
#         # 複数種類のスレッドを同時に実行させたいことがあるかもしれないので、event.set()でタイミングをあわせる予定
#         event.set()  # event.wait()で待機しているプロセスを全部同時に解除
#
#     def record_via_device(self, event: threading.Event, lock: threading.Lock,
#                           file_name: str = '',
#                           received: dict = None,
#                           ext: str = 'h5'):
#         """
#         Parameters
#         ----------
#         event : threading.Event
#         lock : threading.Lock
#         file_name : str
#         received : dict
#             マシニングセンタからシリアル通信で受け取った情報
#         ext : str
#         """
#         logging.debug('record thread start')
#         event.wait()  # event.set()が実行されるまで待機
#         logging.debug('do event')
#         with lock:  # deviceへのアクセスを制限
#             logging.debug('unlock')
#             dt = datetime.now()
#             file_path = self._storage_path + "/{0}.".format(dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext
#             # データの収集(PicoData型)
#             data = self._record_device()
#             # データ(PicoData型)にマシニングから受け取った情報を追加する
#             data.machining_condition = received
#             # 取得したデータがオーバーフローしていないかなどを確認
#             self.pico_data_errror_check(data, file_path)
#             # データの保存
#             self._file_save_tool(data, file_path)
#
#         logging.debug('record end')
#
#     def pico_data_errror_check(self, picodata: PicoData, info: str):
#         """
#         Parameters
#         ----------
#         picodata : PicoData
#         info : str
#         """
#
#         assigned_dict = picodata.assigned_values
#         if not assigned_dict:
#             return
#
#         over_flow = assigned_dict.get('over_flow')
#         trg_auto_ms = assigned_dict.get('trigger_auto_ms')
#         run_time_ms = assigned_dict.get('run_block_time_ms')
#         run_elp_time_sec = assigned_dict.get('run_block_elapsed_time_sec')
#
#         if over_flow:
#             logger.error('over flow at :' + info)
#
#     @staticmethod
#     def create_storage_directory(dir_path: str = None) -> str:
#         """
#         インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
#         Parameters
#         ----------
#         dir_path : str
#             配置するディレクトリ
#             （指定しない場合はカレントディレクトリになる）
#
#         Returns
#         -------
#         str
#             生成したディレクトリのパス
#             （配置するディレクトリは自動生成される）
#
#         """
#         p_dir_path = Path(dir_path) if dir_path else Path.cwd()
#
#         dt = datetime.now()
#         y = dt.strftime('%Y')
#         m = dt.strftime('%m')
#         d = dt.strftime('%d')
#
#         p_dir_path = p_dir_path / y / m / d
#
#         # ディレクトリを生成
#         # exist_ok : 既に存在していてもエラーにならない
#         # parents : 階層が深くても再帰的に生成
#         p_dir_path.mkdir(exist_ok=True, parents=True)
#
#         return str(p_dir_path.resolve())


if __name__ == "__main__":
    """ 動作確認用
    measure_manager.pyを単体で実行する場合は、
    Terminalでserial_listenerの位置にいる状態から「-m」オプションで実行しないといけない
    （相対インポートをしているので「.py」をつけずにモジュールとして実行）
    python -m util.measure_mamager

    ちなみに、下記のように*.pyファイルとして実行すると相対パスで指定したモジュールを
    Importできずにエラーとなる
    python util\measure_manager.py

    ImportError: attempted relative import with no known parent package

    参考
    https://note.nkmk.me/python-relative-import/
    """

    # import time
    # from .measure.pico5000_capture import Ps5000Block
    #
    # threshold = 500  # [mv]
    # samples = 2 ** 19
    #
    # dev = Ps5000Block(th_mv=threshold, num_samples=samples)
    #
    # dm = DataRecordManager(dev)
    #
    # # ダミーの受信データ
    # rec_dict = {'AC': 10.0, 'ST': 5.0, 'ZP': -0.08}
    #
    # dm.device_open()  # DataManagerが保持してるデバイスを開いて計測準備を行う
    # dm(rec_dict)  # 計測の実行 -> PicoData型（自作のデータ型）-> joblib で保存
    #
    # time.sleep(15)  # 計測が実行される前にcloseするとエラーになるので待機
    # dm.device_close()  # DataManagerが保持してるデバイスを閉じる

    import time
    # from .measure.pico5000_capture import Ps5000Block
    from .measure.pico2000_capture import Ps2000Block
    from .measure.cam_capture import CaptureDevice

    threshold = 500  # [mv]
    samples = 2 ** 19
    pico_dev = Ps2000Block(th_mv=threshold, num_samples=samples)
    pm = PicoManagerBuilder(pico_dev)
    pd = RecordManagerDirector(pm)

    cap_dev = CaptureDevice(cam_num=0)
    cm = CaptureManagerBuilder(cap_dev)
    cd = RecordManagerDirector(cm)

    # ダミーの受信データ
    rec_dict = {'AC': 10.0, 'ST': 5.0, 'ZP': -0.08, }
    # rec_dict = {'CC': 10.0, }

    pd.device_open()  # DataManagerが保持してるデバイスを開いて計測準備を行う
    cd.device_open()  # DataManagerが保持してるデバイスを開いて計測準備を行う

    pd(rec_dict)  # 計測の実行
    cd(rec_dict)  # 計測の実行

    time.sleep(15)  # 計測が実行される前にcloseするとエラーになるので待機

    print('close')
    pd.device_close()  # DataManagerが保持してるデバイスを閉じる
    cd.device_close()  # DataManagerが保持してるデバイスを閉じる
