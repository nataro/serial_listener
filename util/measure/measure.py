import dataclasses
from abc import ABCMeta, abstractmethod
from functools import partial
from pathlib import Path

import h5py
import numpy


class CallableMeasurement(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, **kwargs):  # data_load
        """Retrieve data from the input source and return an object."""
        return

    @abstractmethod
    def open(self):
        """Open the measurement device."""
        pass

    @abstractmethod
    def close(self):
        """Close the measurement device.
        """
        pass


@dataclasses.dataclass
class PicoData(object):
    """
    データを格納する専用のデータセット
    python>=3.7
    Attributes
        data : numpy.ndarray
            (電圧[mv])
        time : numpy.ndarray
            (時間軸[ns])
        assigned_values : dict
            pico-scopeの設定値
        status_values : dict
            pico-scope実行時のステータス
        machining_condition : dict
            切削加工の条件などを格納する
    """
    data: numpy.ndarray
    time: numpy.ndarray
    assigned_values: dict
    status_values: dict
    machining_condition: dict


class PicoDataH5FileSave(object):
    """
    自作したdataclassをそのままjoblib等で保存すると、load時に自作dataclassモジュール
    の参照ができずに厄介なことになる。
    なので、HDF5として保存することにする。

    データのロードは
    mv = h5['pico5000']['millivolt']
    print(mv)  # これはオブジェクトのまま
    print(mv[()])  # [()]で配列全体をロードできる

    アノテーションのロードは
    attr = h5['pico5000'].attrs
    for keys, values in attr.items():
        print(keys, ' : ', values)
    """

    def __init__(self, save_file_path: str = None):
        self._method = {
            '.hdf5': partial(h5py.File, mode='w'),
            '.h5': partial(h5py.File, mode='w'),
        }
        self._save_file_path = save_file_path

    def suffix_check(self, file_path: str):
        suffix = Path(file_path).suffix
        assert suffix, 'Provide suffix of output file!'
        assert suffix in self._method.keys(), "Suffix must be '.hdf5' or '.h5'. But present value is {0}".format(
            suffix)
        return suffix

    def __call__(self, pico_data: PicoData, save_file_path: str = None):
        """
        Parameters
        ----------
        pico_data : PicoData
        save_file_path : str
        """

        file_path = save_file_path if save_file_path else self._save_file_path

        assert file_path, "Provide output filename with '.hdf5' or '.h5' suffix!"

        dump_method = self._method.get(self.suffix_check(file_path))

        with dump_method(file_path) as h5:
            pico_group = h5.create_group('pico5000')
            machining_group = h5.create_group('machining')
            millivolt_dataset = pico_group.create_dataset('millivolt', data=pico_data.data)
            time_dataset = pico_group.create_dataset('time', data=pico_data.time)

            # 各種設定情報などをhdf5のattrsとして格納
            if pico_data.assigned_values:
                for k, v in pico_data.assigned_values.items():
                    pico_group.attrs[k] = v

            if pico_data.status_values:
                for k, v in pico_data.status_values.items():
                    pico_group.attrs[k] = v

            if pico_data.machining_condition:
                for k, v in pico_data.machining_condition.items():
                    machining_group.attrs[k] = v
