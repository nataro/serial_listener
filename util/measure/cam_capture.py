import time
from typing import Optional

import cv2
import numpy as np

from .measure import CallableMeasurement


class CaptureDevice(CallableMeasurement):
    """
    カメラデバイスはcall()で呼び出して撮影を行うときに、デバイスを自動でopenし、
    終了後に自動でcloseするような運用をするので、self.open()やself.close()による
    操作は無効になるように実装している
    """

    def __init__(self, cam_num: int = 0):
        self._cam_num = cam_num
        self._video_cap = None
        self._width = 1280
        self._height = 1024
        self._fps = 15

    def __call__(self, **kwargs) -> Optional[np.ndarray]:
        is_open = self.open_cam()
        if is_open:
            # 50フレームくらい回して安定させる
            for _ in range(50):
                ret, frame = self._video_cap.read()
            # open、closeを何度か実行しないとLEDライトが確実に切れない
            self.close_cam()
            self.open_cam()
            self.close_cam()
            time.sleep(1)  # カメラが安定するまでしばらく待機
            if ret:
                return frame
        return None

    def open(self) -> bool:
        """
        カメラは実行時に自動的にopenし、撮影が終わったらcloseするように運用する
        外部から操作できないようにこの関数は常にFalseを返すようにしておく
        """
        return False

    def close(self) -> None:
        """
        カメラは実行時に自動的にopenし、撮影が終わったらcloseするように運用する
        外部から操作できないようにこの関数は常にNoneを返すようにしておく
        """
        return None

    def open_cam(self):
        self._video_cap = self.cam_cpture()
        is_open = False if self._video_cap is None else self._video_cap.isOpened()
        return is_open

    def cam_cpture(self) -> cv2.VideoCapture:
        # cap = cv2.VideoCapture(self._cam_num, cv2.CAP_DSHOW)
        # cv2.CAP_DSHOWを付けないと[ WARN:0] global ... async callback
        # というワーニングが出るが、cv2.CAP_DSHOWにすると画像の明るさが低下する
        cap = cv2.VideoCapture(self._cam_num)
        # cap = cv2.VideoCapture(self._cam_num, cv2.CAP_MSMF)

        # フォーマット・解像度・FPSの設定
        # cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G'))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, self._width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self._height)
        cap.set(cv2.CAP_PROP_FPS, self._fps)

        return cap

    def close_cam(self) -> None:
        if self._video_cap:
            self._video_cap.release()
            cv2.destroyAllWindows()  # Handles the releasing of the camera accordingly
