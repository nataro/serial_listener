import ctypes
import math
import time  # RunBlockにおける経過時間計測に利用する

import numpy as np
from picosdk.functions import adc2mV, assert_pico_ok, mV2adc
from picosdk.ps5000a import ps5000a as ps

from .measure import CallableMeasurement, PicoData, PicoDataH5FileSave


class Ps5000aUtil(object):
    """
    PS5000Aに関する列挙型の中身を確認したいときに使う。
    また、PS5000A_DEVICE_RESOLUTIONによって変化するtimebase（掃引時間）の計算式を取得できる。

        # 列挙型の確認
        Ps5000aUtil.enume_list()

        # デバイス分解能の種類の確認
        resolution_enume = Ps5000aUtil.get_enume('PS5000A_DEVICE_RESOLUTION')

    4ch使用する場合は、解像度は14bitまで（125MS/sec）
    2ch使用する場合は、解像度は15bitまで（125MS/sec）
    1chしか使用しない場合は、解像度16bitまで（62.5MS/sec）
    """
    ps5000a_enumeration = {
        'PS5000A_DEVICE_RESOLUTION': ps.PS5000A_DEVICE_RESOLUTION,
        'PS5000A_PS5000A_COUPLING': ps.PS5000A_COUPLING,
        'PS5000A_PS5000A_CHANNEL': ps.PS5000A_CHANNEL,
        'PS5000A_PS5000A_RANGE': ps.PS5000A_RANGE,
        'PS5000A_THRESHOLD_DIRECTION': ps.PS5000A_THRESHOLD_DIRECTION,
        'PS5000A_DIGITAL_CHANNEL': ps.PS5000A_DIGITAL_CHANNEL,
        'PS5000A_DIGITAL_DIRECTION': ps.PS5000A_DIGITAL_DIRECTION,
        'PS5000A_THRESHOLD_MODE': ps.PS5000A_THRESHOLD_MODE,
        'PS5000A_TRIGGER_STATE': ps.PS5000A_TRIGGER_STATE,
        'PS5000A_RATIO_MODE': ps.PS5000A_RATIO_MODE,
        'PS5000A_TIME_UNITS': ps.PS5000A_TIME_UNITS,
    }

    def __init__(self):
        pass

    @classmethod
    def enume_list(cls):
        return list(cls.ps5000a_enumeration.keys())

    @classmethod
    def get_enume(cls, enum_name: str):
        return cls.ps5000a_enumeration.get(enum_name, {})

    @staticmethod
    def time_base_n2sec_8bit(n: int):
        assert n >= 1, 'In 8bit mode, timebese must be 1 or more.'
        assert n <= (2 ** 32 - 1), 'In 8bit mode, timebese must be 2^32 - 1 or less.'
        if n <= 2:
            return 2 ** n / 1000000000
        else:
            return (n - 2) / 125000000

    @staticmethod
    def time_base_sec2n_8bit(sec: float):
        assert sec >= 2e-9, 'In 8bit mode, sampling period must be 2ns or more.'
        assert sec <= 34.36, 'In 8bit mode, sampling period must be 34.36s or less.'
        if sec < 8e-9:  # 8nsec以下
            return int(math.log2(sec * 1000000000))
        else:
            return int(sec * 125000000) + 2

    @staticmethod
    def time_base_n2sec_12bit(n: int):
        assert n >= 2, 'In 12bit mode, timebese must be 2 or more.'
        assert n <= (2 ** 32 - 2), 'In 12bit mode, timebese must be 2^32 - 2 or less.'
        if n <= 3:
            return 2 ** (n - 1) / 500000000
        else:
            return (n - 3) / 62500000

    @staticmethod
    def time_base_sec2n_12bit(sec: float):
        assert sec >= 4e-9, 'In 12bit mode, sampling period must be 4ns or more.'
        assert sec <= 68.72, 'In 12bit mode, sampling period must be 68.72s or less.'
        if sec < 16e-9:  # 16nsec以下
            return int(math.log2(sec * 500000000)) + 1
        else:
            return int(sec * 62500000) + 3

    @staticmethod
    def time_base_n2sec_14_15bit(n: int):
        assert n >= 4, 'In 14bit or 15bit mode, timebese must be 4 or more.'
        assert n <= (2 ** 32 - 1), 'In 14bit or 15bit mode, timebese must be 2^32 - 1 or less.'
        return (n - 2) / 125000000

    @staticmethod
    def time_base_sec2n_14_15bit(sec: float):
        assert sec >= 16e-9, 'In 14bit or 15bit mode, sampling period must be 16ns or more.'
        assert sec <= 34.36, 'In 14bit or 15bit mode, sampling period must be 34.36s or less.'
        return int(sec * 125000000) + 2

    @staticmethod
    def time_base_n2sec_16bit(n: int):
        assert n >= 4, 'In 16bit mode, timebese must be 4 or more.'
        assert n <= (2 ** 32 - 2), 'In 16bit mode, timebese must be 2^32 - 2 or less.'
        if n == 4:
            return 1 / 62500000
        else:
            return (n - 3) / 62500000

    @staticmethod
    def time_base_sec2n_16bit(sec: float):
        assert sec >= 16e-9, 'In 16bit mode, sampling period must be 16ns or more.'
        assert sec <= 68.72, 'In 16bit mode, sampling period must be 68.72s or less.'
        if sec < 3.2e-08:  # 32nsec
            return 4
        else:
            return int(sec * 62500000) + 3

    @classmethod
    def sec2timebase_formula(cls, mode: int = 2):
        """
        mode :
            ps.PS5000A_DEVICE_RESOLUTIONのモードの値を入れる
                {'PS5000A_DR_8BIT': 0,
                 'PS5000A_DR_12BIT': 1,
                 'PS5000A_DR_14BIT': 2,
                 'PS5000A_DR_15BIT': 3,
                 'PS5000A_DR_16BIT': 4}
        """

        formula_dict = {
            0: cls.time_base_sec2n_8bit,
            1: cls.time_base_sec2n_12bit,
            2: cls.time_base_sec2n_14_15bit,
            3: cls.time_base_sec2n_14_15bit,
            4: cls.time_base_sec2n_16bit,
        }

        return formula_dict.get(mode, None)


class Ps5000Block(CallableMeasurement):
    """
    See below for enums.
    picosdk-python-wrappers/picosdk/ps5000a.py
    https://github.com/picotech/picosdk-python-wrappers/blob/master/picosdk/ps5000a.py

        th_mv : トリガーの閾値[mV]
        period_nsec : サンプリング周期[sec]
        num_samples : サンプリング個数
        pre_samples : トリガー前のサンプルを何個取得するか
        auto_trigger_time_ms: オートトリガーが作動するまでの時間
        resolution_mode: 計測値の分解能を指定　例 'PS5000A_DR_14BIT'
        range_mode: 計測値のレンジを指定　例 'PS5000A_2V'

    を指定して計測を行う
    """

    def __init__(self,
                 th_mv: float = 100,
                 period_nsec: int = 500,
                 num_samples: int = 2 ** 15,
                 pre_samples: int = 0,
                 auto_trigger_time_ms: int = 5000,
                 resolution_mode: str = 'PS5000A_DR_14BIT',
                 range_mode: str = 'PS5000A_2V'
                 ):

        self._trigger_th_mv = th_mv  # トリガーの閾値をmvで指定
        self._specified_timebase_sec = period_nsec * 1.0e-9  # 掃引時間の指示時間（その通り設定される訳ではない） 500ns
        self._num_samples = num_samples  # 取得するサンプル数の指示 2**15=32768
        self._trigger_pre_samples = pre_samples  # トリガーの前方の何個のデータを取得するか
        self._trigger_post_samples = self._num_samples - self._trigger_pre_samples  # トリガーの後方のデータ数
        self._trigger_auto_ms = auto_trigger_time_ms  # オートトリガーが掛かるまでの時間[msec]

        self._resolution = ps.PS5000A_DEVICE_RESOLUTION[resolution_mode]  # 計測値の分解能
        self.time_base_formula_sec2n = Ps5000aUtil.sec2timebase_formula(self._resolution)  # timebaseの計算式
        self._segment_index = 0  # 使用するメモリのインデックス番号
        self._coupling_type = ps.PS5000A_COUPLING["PS5000A_DC"]
        self._read_ch1 = ps.PS5000A_CHANNEL["PS5000A_CHANNEL_A"]  # 計測用のチャンネル
        self._ch1_range = ps.PS5000A_RANGE[range_mode]  # 計測用のチャンネルのレンジ
        self._downsample_ratio_mode = ps.PS5000A_RATIO_MODE['PS5000A_RATIO_MODE_NONE']  # RATIO_MODE_NONE = 0
        self._trigger_ch = self._read_ch1  # トリガー用のチャンネル
        self._trigger_range = self._ch1_range  # トリガー用のチャンネルのレンジ
        self._trigger_direction = ps.PS5000A_THRESHOLD_DIRECTION["PS5000A_RISING"]  # トリガーの方向
        self._run_block_elapsed_time_sec = 0  # 待機時間も含めてRunBlockの実行時に経過した時間を計測

        self._status = {}  # pico-scopeの作動状態を格納する
        self._assigned_values = {}  # pico-scopeの設定値を格納する
        self._c_handle = ctypes.c_int16()  # 起動したpicoscopeの個体識別に用いるhandle
        self._c_max_ADC = ctypes.c_int16()  # ADC値の最大値
        self._c_time_interval_ns = ctypes.c_float()  # 実際に取得するデータの時間間隔がnsecで入る
        self._c_available_max_samples = ctypes.c_int32()  # 取得可能な最大サンプル数が格納される
        self._c_sampled_num = ctypes.c_int32()  # ps5000aGetValues時に指定し、取得した実際のサンプル数が返ってくる
        self._c_time_indisposed_ms = ctypes.c_int32()  # 計測に掛かった時間がmsecで格納される(待機時間は含まれない)
        self._c_over_flow = ctypes.c_int16()  # データ回収時にオーバーフローが起きたか確認(通常:0、レンジオーバー:1)

        # 計測データを格納するバッファ
        self._buffer_ch1_max = (ctypes.c_int16 * self._num_samples)()
        self._buffer_ch1_min = (
                ctypes.c_int16 * self._num_samples)()  # used for downsampling which isn't in the scope of this example

    def __call__(self, **kwargs) -> PicoData:
        assert self.check_set_values(), 'device does not open'

        self.run_block_mode()
        self.set_data_buffers()
        self.retrieve_block_mode_data()
        # self.draw_graph()

        return self.retrieve_picodata()

    def open(self):
        self.open_scope()

    def close(self):
        self.close_scope()

    def open_scope(self):
        """
        pico-scopeを開き、計測条件の設定などを実施する
        """
        self.open_unit()
        self.set_up_input_channels()
        self.get_maximum_adc_count()
        self.set_trigger()
        self.get_timebase_2()

        for keys, values in self._assigned_values.items():
            print(keys, ' : ', values)

    def close_scope(self):
        """
        pico-scopeの停止処理
        """
        # Stop the scope
        self._status["stop"] = ps.ps5000aStop(self._c_handle)
        assert_pico_ok(self._status["stop"])

        # Close unit Disconnect the scope
        self._status["close"] = ps.ps5000aCloseUnit(self._c_handle)
        assert_pico_ok(self._status["close"])

    def check_open_status(self):
        """
        デバイスが開いており、掃引時間やトリガ設定が完了しているか確認
        """
        if self._status.get('open_unit', None) is None:
            return False
        if self._status.get('set_ch1', None) is not 0:
            return False
        if self._status.get('trigger', None) is not 0:
            return False
        if self._status.get('get_tame_base_2', None) is not 0:
            return False

        return True

    def open_unit(self):
        """
        (P69)
        picoscopeをopenし、API使用時に個体識別番号として用いるhandleを取得する

        PICO_STATUS ps5000aOpenUnit
        (
            int16_t * handle, // 開いた際にscopeのid番号のようなものがここに渡される
            int8_t * serial  // ここがNULLの場合、見つかった最初のscopeが開かれる
            PS5000A_DEVICE_RESOLUTION resolution
        )
        """
        self._status['open_unit'] = ps.ps5000aOpenUnit(
            ctypes.byref(self._c_handle),
            None,
            self._resolution
        )

        try:
            assert_pico_ok(self._status['open_unit'])
        except:  # PicoNotOkEroor
            power_status = self._status['open_unit']

            if power_status == 286:
                self._status['change_power_source'] = ps.ps5000aChangePowerSource(self._c_handle, power_status)
            elif power_status == 282:
                self._status['change_power_source'] = ps.ps5000aChangePowerSource(self._c_handle, power_status)
            else:
                raise

            assert_pico_ok(self._status['change_power_source'])

    def set_up_input_channels(self):
        """
        set up input channels(P78)
        チャンネルのレンジやオフセット、カップリング等を指定する

        PICO_STATUS ps5000aSetChannel
        (
            int16_t handle,    // ps5000aOpenUnitで取得したhandleを渡す
            PS5000A_CHANNEL channel, // 設定するチャンネルの指定
            int16_t enabled,  // enableにするときは「1」
            PS5000A_COUPLING type, // カップリング指定
            PS5000A_RANGE range, // 計測レンジ指定
            float analogueOffset  // 値をオフセットする場合にここに入力
        )
        """
        enable = 1
        analogue_offset = 0  # 0V 単位はボルト？
        self._status['set_ch1'] = ps.ps5000aSetChannel(
            self._c_handle,
            self._read_ch1,
            enable,
            self._coupling_type,
            self._ch1_range,
            analogue_offset
        )
        assert_pico_ok(self._status['set_ch1'])

    def get_maximum_adc_count(self):
        """
        get the maximum ADC count(P64)
        設定状態におけるmaximum ADC count valueを取得する
        (ADCはADコンバータ値という意味だと思う)
        """
        self._status['maximum_value'] = ps.ps5000aMaximumValue(
            self._c_handle,
            ctypes.byref(self._c_max_ADC)
        )
        assert_pico_ok(self._status['maximum_value'])

    def set_trigger(self):
        """
        set edge or level trigger(p109)
        トリガーの設定を行う。閾値がADCカウント値であることに注意。
        PICO_STATUS ps5000aSetSimpleTrigger
        (
            int16_t handle, // ps5000aOpenUnitで取得したhandle
            int16_t enable, // 「0」はトリガーを使わないということ
            PS5000A_CHANNEL source,  // トリガーの対象とするチャンネル
            int16_t threshold, // the ADC count at which the trigger will fire.
            PS5000A_THRESHOLD_DIRECTION direction,  // トリガーの方向
            uint32_t delay,
            int16_t autoTrigger_ms // ここで指定した時間が経過した場合は強制的に計測実行する
        )

        direction
            ABOVE, BELOW, RISING, FALLING and RISING_OR_FALLING.

        delay
            the time between the trigger occurring and the first sample. For example,
            if delay = 100, the scope would wait 100 sample periods before sampling.

        """
        enabled = 1
        threshold = int(mV2adc(self._trigger_th_mv, self._trigger_range, self._c_max_ADC))
        delay = 0
        self._status['trigger'] = ps.ps5000aSetSimpleTrigger(
            self._c_handle,
            enabled,
            self._trigger_ch,
            threshold,
            self._trigger_direction,
            delay,
            self._trigger_auto_ms
        )
        assert_pico_ok(self._status['trigger'])

    def get_timebase_2(self):
        """
        掃引時間の設定
        get properties of the selected timebase (p44)
        PICO_STATUS ps5000aGetTimebase2
        (
            int16_t handle,
            uint32_t timebase,
            int32_t noSamples, // the number of samples required.(取得するサンプル数)
            float * timeIntervalNanoseconds, // 実行後、タイムインターバルがここに入る
            int32_t * maxSamples, // 実行後に、取得できる最大サンプル数がここに入る
            uint32_t segmentIndex  // the index of the memory segment to use.
        )

        """
        timebase_n = self.time_base_formula_sec2n(self._specified_timebase_sec)

        self._status['get_tame_base_2'] = ps.ps5000aGetTimebase2(
            self._c_handle,
            timebase_n,
            self._num_samples,
            ctypes.byref(self._c_time_interval_ns),
            ctypes.byref(self._c_available_max_samples),
            self._segment_index
        )
        assert_pico_ok(self._status['get_tame_base_2'])

        # 設定値の確認
        # 時間に関する設定値は掃引時間を取得した後に確認しないと意味がない
        self._assigned_values = self.check_set_values()

    def check_set_values(self) -> dict:
        """
        各種設定内容を確認するためもの
        """
        if not self.check_open_status():
            print('At first, please open picoscope and configure settings !')

        set_values = {}

        coupling_keys = [k for k, v in ps.PS5000A_COUPLING.items() if v == self._coupling_type]
        resolution_keys = [k for k, v in ps.PS5000A_DEVICE_RESOLUTION.items() if v == self._resolution]
        range_keys = [k for k, v in ps.PS5000A_RANGE.items() if v == self._ch1_range]
        direction_keys = [k for k, v in ps.PS5000A_THRESHOLD_DIRECTION.items() if v == self._trigger_direction]

        set_values['ch1_coupling'] = coupling_keys
        set_values['ch1_resolution'] = resolution_keys
        set_values['ch1_range'] = range_keys
        set_values['trigger_threshold_mV'] = self._trigger_th_mv
        set_values['trigger_direction'] = direction_keys
        set_values['trigger_auto_ms'] = self._trigger_auto_ms

        # 時間に関する情報は掃引時間取得後に実行しないと意味がないと思う
        t_interval_ns = self._c_time_interval_ns.value
        # available_max_samples = self._c_available_max_samples.value
        set_num_samples = self._num_samples
        set_total_time = set_num_samples * t_interval_ns

        set_values['sampring_period_nsec'] = t_interval_ns
        set_values['sampring_rate_Hz'] = 1.0 / (t_interval_ns * 1.0e-9)
        set_values['set_num_samples'] = set_num_samples
        set_values['set_pre_samples'] = self._trigger_pre_samples
        set_values['set_post_samples'] = self._trigger_post_samples
        set_values['set_total_time_ns'] = set_total_time

        # Runblockの実行に掛かった時間は、実際に計測した後に確認しないと意味がないと思う
        set_values['run_block_time_ms'] = self._c_time_indisposed_ms.value
        set_values['run_block_elapsed_time_sec'] = self._run_block_elapsed_time_sec
        set_values['over_flow'] = self._c_over_flow.value

        return set_values

    def run_block_mode(self):
        """
        start block mode(p74)
        ブロックモード（バッファメモリを使用）でのデータ収集を実行する
        PICO_STATUS ps5000aRunBlock
        (
            int16_t handle,
            int32_t noOfPreTriggerSamples, // トリガーの前方の何個のデータを取得するか
            int32_t noOfPostTriggerSamples,  // トリガーの後方の何個のデータを取得するか
            uint32_t timebase,
            int32_t * timeIndisposedMs, // 実行後に、計測に掛かった時間がmsecで格納される
            uint32_t segmentIndex,  // 使用するメモリインデックスの番号？
            ps5000aBlockReady lpReady,  // データ取得の準備が出来ているか確認する
            void * pParameter  // 準備が出来次第コールバック関数を呼び出したいときにここに設定する？
        )

        """
        timebase_n = self.time_base_formula_sec2n(self._specified_timebase_sec)
        # time_indisposed = None
        ip_ready = None  # using ps5000aIsReady rather than ps5000aBlockReady
        # ↑データ取得の準備が出来ているか確認するためのものだが、これよりもps2000aIsReady()で確認するべき
        p_parameter = None
        # ↑準備が出来次第コールバック関数を呼び出したいときにここに設定する？

        self._status['run_block'] = ps.ps5000aRunBlock(
            self._c_handle,
            self._trigger_pre_samples,
            self._trigger_post_samples,
            timebase_n,
            # time_indisposed,
            ctypes.byref(self._c_time_indisposed_ms),
            self._segment_index,
            ip_ready,
            p_parameter
        )
        assert_pico_ok(self._status['run_block'])

        # ここでトリガー待ちの状態になる。
        # トリガーが掛かりデータ取得が終了すると、再びデータ取得可能な状態になるので、
        # ↓のようにwhileループを回しながら、ps****IsReady()で確認している

        # 経過時間の計測開始
        self._run_block_elapsed_time_sec = 0
        start = time.time()

        # Check for data collection to finish using ps5000aIsReady
        ready = ctypes.c_int16(0)
        check = ctypes.c_int16(0)
        while ready.value == check.value:
            self._status['is_ready'] = ps.ps5000aIsReady(self._c_handle, ctypes.byref(ready))

        # 経過時間の計測終了
        end = time.time()
        self._run_block_elapsed_time_sec = end - start
        # print(self._run_block_elapsed_time_sec)

    def set_data_buffers(self):
        """
        register aggregated data buffers with driver(p81)
        どこにデータを保存するか登録する
        If you do not need two buffers, because you are not using aggregate mode,
        then you can optionally use ps5000aSetDataBuffer instead.

        PICO_STATUS ps5000aSetDataBuffers
        (
            int16_t handle,
            PS5000A_CHANNEL source,
            int16_t * bufferMax,  // ここに欲しい値が格納されるのだと思う
            int16_t * bufferMin,  // aggregate mode のときのみ利用する？？
            int32_t bufferLth, // bufferMaxとbufferMin配列のlength（データの個数）
            uint32_t segmentIndex, // 使用するメモリインデックスの番号？
            PS5000A_RATIO_MODE mode // ダウンサンプリングの適用や方法を指定（0のときはしない）
        )
        """
        self._status['set_data_buffer_ch1'] = ps.ps5000aSetDataBuffers(
            self._c_handle,
            self._read_ch1,
            ctypes.byref(self._buffer_ch1_max),
            ctypes.byref(self._buffer_ch1_min),
            self._num_samples,
            self._segment_index,
            self._downsample_ratio_mode
        )
        assert_pico_ok(self._status['set_data_buffer_ch1'])

    def retrieve_block_mode_data(self):
        """
        retrieve block-mode data with callback(p52)
        コールバックでブロックモードのデータを回収する
        PICO_STATUS ps5000aGetValues
        (
            int16_t handle,
            uint32_t startIndex,  // どのインデックスから回収するか
            uint32_t * noOfSamples, // 要求するサンプル数を入力し、実行後には、実際のサンプル数が格納される
            uint32_t downSampleRatio, // ダウンサンプリングを実施する場合の係数？
            PS5000A_RATIO_MODE downSampleRatioMode, // ダウンサンプリングの適用や方法を指定（0のときはしない）
            uint32_t segmentIndex, // 使用するメモリインデックスの番号？
            int16_t * overflow  // over voltage が起きたかどうか（y軸のレンジオーバーのことか？）
        )
        """
        start_index = 0
        # ↓要求するサンプル数を入力し、実行後には、実際のサンプル数が格納される
        self._c_sampled_num = ctypes.c_int32(self._num_samples)
        downsample_ratio = 0
        # over_flow = ctypes.c_int16()

        self._status['get_values'] = ps.ps5000aGetValues(
            self._c_handle,
            start_index,
            ctypes.byref(self._c_sampled_num),
            downsample_ratio,
            self._downsample_ratio_mode,
            self._segment_index,
            # ctypes.byref(over_flow))
            ctypes.byref(self._c_over_flow))
        assert_pico_ok(self._status['get_values'])

    def retrieve_picodata(self) -> PicoData:
        """
        以下の属性を持つdataclass型のデータを返す
            data : numpy.ndarray
                (電圧[mv])
            time : numpy.ndarray
                (時間軸[ns])
            assigned_values : dict
                pico-scopeの設定値
            status_values : dict
                pico-scope実行時のステータス
            machining_condition : dict
                切削加工の条件などを格納する

        Returns
        -------
        PicoData
        """
        # convert ADC counts data to mV
        # ADC値をmVに変換
        adc2mv_ch1 = adc2mV(
            self._buffer_ch1_max,
            self._ch1_range,
            self._c_max_ADC
        )

        # Create time data
        # 時間軸のデータを生成
        time_data = np.linspace(
            0,
            self._c_sampled_num.value * self._c_time_interval_ns.value,
            self._c_sampled_num.value
        )

        # 計測完了後に self._c_time_indisposed_ms の値を受け取るために更新する
        self._assigned_values = self.check_set_values()

        ''' 切削加工条件に関するデータはこの時点では格納しない '''
        picodata = PicoData(
            data=np.asarray([adc2mv_ch1]),
            time=time_data,
            assigned_values=self._assigned_values,
            status_values=self._status,
            machining_condition=None
        )

        return picodata

    # def draw_graph(self):
    #     """
    #     これは確認用のおまけ
    #     """
    #     import matplotlib.pyplot as plt
    #
    #     # convert ADC counts data to mV
    #     # ADC値をmVに変換
    #     adc2mv_ch1_max = adc2mV(
    #         self._buffer_ch1_max,
    #         self._ch1_range,
    #         self._c_max_ADC
    #     )
    #
    #     # Create time data
    #     # 時間軸のデータを生成
    #     time_data = np.linspace(
    #         0,
    #         self._c_sampled_num.value * self._c_time_interval_ns.value,
    #         self._c_sampled_num.value
    #     )
    #
    #     # plot data from channel A
    #     plt.plot(time_data, adc2mv_ch1_max[:])
    #     plt.xlabel('Time (ns)')
    #     plt.ylabel('Voltage (mV)')
    #     plt.show()


if __name__ == "__main__":
    """ 動作確認用
    pico5000_capture.pyを単体で実行する場合は、
    Terminalでserial_listenerの位置にいる状態から「-m」オプションで実行しないといけない
    （相対インポートをしているので「.py」をつけずにモジュールとして実行）
    python -m util.measure.pico5000_capture
    
    ちなみに、下記のように*.pyファイルとして実行すると相対パスで指定したモジュールを
    Importできずにエラーとなる
    python util\measure\pico5000_capture.py
    
    ImportError: attempted relative import with no known parent package
    
    参考
    https://note.nkmk.me/python-relative-import/
    """
    save_tool = PicoDataH5FileSave()

    threshold = 500  # [mv]
    samples = 2 ** 19

    dev = Ps5000Block(th_mv=threshold, num_samples=samples)

    dev.open()  # デバイスを開いて計測準備を行う
    data = dev()  # 計測の実行 -> PicoData型（自作のデータ型）
    save_tool(data, 'test.h5')  # データの保存
    dev.close()  # デバイスを閉じて計測終了
