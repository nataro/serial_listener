import ctypes

import numpy as np
from picosdk.functions import adc2mV, assert_pico_ok
from picosdk.ps2000a import ps2000a as ps

from util.measure.measure import CallableMeasurement, PicoData


class Ps2000Block(CallableMeasurement):
    def __init__(self,
                 th_mv: float = 100,
                 period_nsec: int = 500,
                 num_samples: int = 2 ** 12,
                 pre_samples: int = 0,
                 auto_trigger_time_ms: int = 50,
                 # resolution_mode: str = 'PS5000A_DR_14BIT',
                 # range_mode: str = 'PS5000A_2V'
                 ):

        self._timebase_n = 800

        self._trigger_th_mv = th_mv  # トリガーの閾値をmvで指定
        self._specified_timebase_sec = period_nsec * 1.0e-9  # 掃引時間の指示時間（その通り設定される訳ではない） 500ns
        self._num_samples = num_samples  # 取得するサンプル数の指示 2**15=32768
        self._trigger_pre_samples = pre_samples  # トリガーの前方の何個のデータを取得するか
        self._trigger_post_samples = self._num_samples - self._trigger_pre_samples  # トリガーの後方の何個のデータを取得するか
        self._trigger_auto_ms = auto_trigger_time_ms  # オートトリガーが掛かるまでの時間[msec]

        # self._resolution = ps.PS5000A_DEVICE_RESOLUTION[resolution_mode]  # 計測値の分解能
        # self.time_base_formula_sec2n = Ps5000aUtil.sec2timebase_formula(self._resolution)  # timebaseの計算式
        self._segment_index = 0  # 使用するメモリのインデックス番号
        # self._coupling_type = ps.PS5000A_COUPLING["PS5000A_DC"]
        self._coupling_type = 1
        # self._read_ch1 = ps.PS5000A_CHANNEL["PS5000A_CHANNEL_A"]  # 計測用のチャンネル
        self._read_ch1 = 0  # 計測用のチャンネル
        # self._ch1_range = ps.PS5000A_RANGE[range_mode]  # 計測用のチャンネルのレンジ
        self._ch1_range = 7  # 計測用のチャンネルのレンジ
        # self._downsample_ratio_mode = ps.PS5000A_RATIO_MODE['PS5000A_RATIO_MODE_NONE']  # RATIO_MODE_NONE = 0
        self._downsample_ratio_mode = 0  # RATIO_MODE_NONE = 0
        self._trigger_ch = self._read_ch1  # トリガー用のチャンネル
        self._trigger_range = self._ch1_range  # トリガー用のチャンネルのレンジ
        # self._trigger_direction = ps.PS5000A_THRESHOLD_DIRECTION["PS5000A_RISING"]  # トリガーの方向(PS5000A_RISING = 2でも良い)
        self._trigger_direction = 2  # トリガーの方向(PS5000A_RISING = 2でも良い)

        self._status = {}  # pico-scopeの作動状態を格納する
        self._c_handle = ctypes.c_int16()  # 起動したpicoscopeのハンドル
        self._c_max_ADC = ctypes.c_int16()  # ADC値の最大値
        self._c_time_interval_ns = ctypes.c_float()  # 実際に取得するデータの時間間隔がnsecで入る
        self._c_available_max_samples = ctypes.c_int32()  # 取得可能な最大サンプル数が格納される
        self._c_sampled_num = ctypes.c_int32()  # ps5000aGetValues時に指定し、取得した実際のサンプル数が返ってくる

        # 計測データを格納するバッファ
        self._buffer_ch1_max = (ctypes.c_int16 * self._num_samples)()
        self._buffer_ch1_min = (
                ctypes.c_int16 * self._num_samples)()  # used for downsampling which isn't in the scope of this example

    def __call__(self, **kwargs) -> PicoData:
        # if not self.check_open_status():
        #     self.open_scope()

        assert self.check_set_values(), print('device does not open')

        self.run_block_mode()
        self.set_data_buffers()
        self.retrieve_block_mode_data()
        # self.draw_graph()
        return self.retrieve_picodata()

    def check_open_status(self):
        """
        デバイスが開いており、掃引時間やトリガ設定が完了しているか確認
        """
        if self._status.get('open_unit', None) is None:
            return False
        if self._status.get('set_ch1', None) != 0:
            return False
        if self._status.get('trigger', None) != 0:
            return False
        if self._status.get('get_tame_base_2', None) != 0:
            return False

        return True

    def open(self):
        self.open_scope()

    def close(self):
        self.close_scope()

    def open_scope(self):
        """
        pico-scopeを開き、計測条件の設定などを実施する
        """
        self.open_unit()
        self.set_up_input_channels()
        self.set_trigger()
        self.get_timebase_2()

    def close_scope(self):
        """
        pico-scopeの停止処理
        """
        # Stop the scope
        self._status["stop"] = ps.ps2000aStop(self._c_handle)
        assert_pico_ok(self._status["stop"])

        # Close unit Disconnect the scope
        self._status["close"] = ps.ps2000aCloseUnit(self._c_handle)
        assert_pico_ok(self._status["close"])

    def open_unit(self):
        """
        Open 2000 series PicoScope
        picoスコープをopenし、API使用時に個体識別番号として用いるhandleを取得する
        """
        self._status['open_unit'] = ps.ps2000aOpenUnit(
            ctypes.byref(self._c_handle),
            None
        )

        assert_pico_ok(self._status['open_unit'])

    def set_up_input_channels(self):
        """
        ## Set up channel A
        # チャンネルのレンジやオフセット、カップリング等を指定する
        # handle = chandle
        # channel = PS2000A_CHANNEL_A = 0
        # enabled = 1
        # coupling type = PS2000A_DC = 1
        # range = PS2000A_2V = 7
        # analogue offset = -0.9V # 加速度計が無負荷の状態で0.9V位の値になっていたから
        """
        enable = 1
        analogue_offset = 0  # 0V 単位はボルト？
        self._status['set_ch1'] = ps.ps2000aSetChannel(
            self._c_handle,
            0,
            enable,
            self._coupling_type,
            self._ch1_range,
            analogue_offset
        )
        assert_pico_ok(self._status['set_ch1'])

    def set_trigger(self):
        """

        """
        enabled = 1
        threshold = 4096
        delay = 0
        self._status['trigger'] = ps.ps2000aSetSimpleTrigger(
            self._c_handle,
            enabled,
            self._trigger_ch,
            threshold,
            self._trigger_direction,
            delay,
            self._trigger_auto_ms
        )
        assert_pico_ok(self._status['trigger'])

    def get_timebase_2(self):
        """
        ## Get timebase information
        # 水平掃引時間などを算出する。ここで得た値を、後のメモリのバッファ確保や時間軸の算出に用いる？
        # handle = chandle
        # timebase (水平掃引時間の設定)
        # timebase = 800 = 6400 ns = 6.4ms(see Programmer's guide for mre information on timebases)
        #          1GS/s model : sampling interval = (n-2)/125,000,000
        # noSamples = totalSamples
        # pointer to timeIntervalNanoseconds = ctypes.byref(timeIntervalNs) # 水平掃引の実時間？
        # pointer to totalSamples = ctypes.byref(returnedMaxSamples)
        # segment index = 0 # 使用するメモリインデックスの番号？

        """
        # timebase_n = self.time_base_formula_sec2n(self._specified_timebase_sec)
        timebase_n = self._timebase_n
        oversample = ctypes.c_int16(0)
        self._status['get_tame_base_2'] = ps.ps2000aGetTimebase2(
            self._c_handle,
            timebase_n,
            self._num_samples,
            ctypes.byref(self._c_time_interval_ns),
            oversample,
            ctypes.byref(self._c_available_max_samples),
            self._segment_index
        )
        assert_pico_ok(self._status['get_tame_base_2'])

        # 設定値の確認
        set_values = self.check_set_values()
        for keys, values in set_values.items():
            print(keys)
            print(values)

    def check_set_values(self) -> dict:
        """
        各種設定内容を確認するためもの
        """
        if not self.check_open_status():
            print('At first, please open picoscope and configure settings !')

        set_values = {}

        # coupling_keys = [k for k, v in ps.PS5000A_COUPLING.items() if v == self._coupling_type]
        # resolution_keys = [k for k, v in ps.PS5000A_DEVICE_RESOLUTION.items() if v == self._resolution]
        # range_keys = [k for k, v in ps.PS5000A_RANGE.items() if v == self._ch1_range]
        # direction_keys = [k for k, v in ps.PS5000A_THRESHOLD_DIRECTION.items() if v == self._trigger_direction]
        #
        # set_values['ch1_coupling'] = coupling_keys
        # set_values['ch1_resolution'] = resolution_keys
        # set_values['ch1_range'] = range_keys
        # set_values['trigger_threshold_mV'] = self._trigger_th_mv
        # set_values['trigger_direction'] = direction_keys

        # print('ch1_coupling: ', coupling_keys)
        # print('ch1_resolution: ', resolution_keys)
        # print('ch1_range: ', range_keys)
        # print('trigger_threshold_mV: ', self._trigger_th_mv)
        # print('trigger_direction: ', direction_keys)

        # 時間に関する情報は掃引時間取得後に実行しないと意味がないと思う
        t_interval_ns = self._c_time_interval_ns.value
        # available_max_samples = self._c_available_max_samples.value
        set_num_samples = self._num_samples
        set_total_time = set_num_samples * t_interval_ns

        set_values['sampring_period_nsec'] = t_interval_ns
        set_values['sampring_rate_Hz'] = 1.0 / (t_interval_ns * 1.0e-9)
        set_values['set_num_samples'] = set_num_samples
        set_values['set_pre_samples'] = self._trigger_pre_samples
        set_values['set_post_samples'] = self._trigger_post_samples
        set_values['set_total_time_ns'] = set_total_time

        # print('sampring_period_nsec: ', t_interval_ns)
        # print('sampring_rate_Hz ', 1.0 / (t_interval_ns * 1.0e-9))
        # print('set_num_samples: ', set_num_samples)
        # print('( pre: ', self._trigger_pre_samples, '  post: ', self._trigger_post_samples, ' )')
        # print('set_total_time_ns: ', set_total_time)

        return set_values

    def run_block_mode(self):
        """
        ## Run block capture
        # ブロックモード（バッファメモリを使用）でのデータ収集を実行する
        # handle = chandle
        # number of pre-trigger samples = preTriggerSamples # トリガーの前方の何個のデータを取得するか
        # number of post-trigger samples = PostTriggerSamples # トリガーの後方の何個のデータを取得するか
        # timebase (水平掃引時間の設定)
        # oversample = 0 = oversample # 使用しない
        # pointer to time indisposed ms = None (not needed in the example) # 計測にかかった時間を取得するときに利用？
        # segment index = 0 # 使用するメモリインデックスの番号？
        # lpReady = None (using ps2000aIsReady rather than ps2000aBlockReady)
        # ↑データ取得の準備が出来ているか確認するためのものだが、これよりもps2000aIsReady()で確認するべき
        # pointer to pParameter = None # 準備が出来次第コールバック関数を呼び出したいときにここに設定する？
        """
        # timebase_n = self.time_base_formula_sec2n(self._specified_timebase_sec)
        timebase_n = self._timebase_n
        oversample = 0
        time_indisposed = None
        ip_ready = None  # using ps5000aIsReady rather than ps5000aBlockReady
        # ↑データ取得の準備が出来ているか確認するためのものだが、これよりもps2000aIsReady()で確認するべき
        p_parameter = None
        # ↑準備が出来次第コールバック関数を呼び出したいときにここに設定する？
        self._status['run_block'] = ps.ps2000aRunBlock(
            self._c_handle,
            self._trigger_pre_samples,
            self._trigger_post_samples,
            timebase_n,
            oversample,
            time_indisposed,
            self._segment_index,
            ip_ready,
            p_parameter
        )
        assert_pico_ok(self._status['run_block'])

        # ここでトリガー待ちの状態になる。
        # トリガーが掛かりデータ取得が終了すると、再びデータ取得可能な状態になるので、
        # ↓のようにwhileループを回しながら、ps****IsReady()で確認している

        # Check for data collection to finish using ps5000aIsReady
        ready = ctypes.c_int16(0)
        check = ctypes.c_int16(0)
        while ready.value == check.value:
            self._status['is_ready'] = ps.ps2000aIsReady(self._c_handle, ctypes.byref(ready))

    def set_data_buffers(self):
        """
    ## Set data buffer location for data collection from channel A (P72)
    # どこにデータを保存するか登録する
    # handle = chandle
    # source = PS2000A_CHANNEL_A = 0
    # pointer to buffer max = ctypes.byref(bufferDPort0Max)
    # pointer to buffer min = ctypes.byref(bufferDPort0Min)
    # buffer length = totalSamples
    # segment index = 0 # 使用するメモリインデックスの番号？
    # ratio mode = PS2000A_RATIO_MODE_NONE = 0 # ダウンサンプリングのモードを指定？
        """
        self._status['set_data_buffer_ch1'] = ps.ps2000aSetDataBuffers(
            self._c_handle,
            self._read_ch1,
            ctypes.byref(self._buffer_ch1_max),
            ctypes.byref(self._buffer_ch1_min),
            self._num_samples,
            self._segment_index,
            self._downsample_ratio_mode
        )
        assert_pico_ok(self._status['set_data_buffer_ch1'])

    def retrieve_block_mode_data(self):
        """
        ## Retried data from scope to buffers assigned above (P43)
        # ブロックモードで得たデータを取得する
        # handle = chandle
        # start index = 0
        # pointer to number of samples = ctypes.byref(cTotalSamples) # 取得するデータの数をポインタで渡す
        # downsample ratio = 0
        # downsample ratio mode = PS2000A_RATIO_MODE_NONE = ダウンサンプリングを無視するモード
        # pointer to overflow = ctypes.byref(overflow))
        """
        start_index = 0
        # ↓要求するサンプル数を入力し、実行後には、実際のサンプル数が格納される
        self._c_sampled_num = ctypes.c_int32(self._num_samples)
        downsample_ratio = 0
        over_flow = ctypes.c_int16()

        self._status['get_values'] = ps.ps2000aGetValues(
            self._c_handle,
            start_index,
            ctypes.byref(self._c_sampled_num),
            downsample_ratio,
            self._downsample_ratio_mode,
            self._segment_index,
            ctypes.byref(over_flow))
        assert_pico_ok(self._status['get_values'])

    def retrieve_picodata(self) -> PicoData:
        """
        以下の属性を持つdataclass型のデータを返す
        Attributes
        data : numpy.ndarray
            (電圧[mv])
        time : numpy.ndarray
            (時間軸[ns])
        set_values : dict
            pico-scopeの設定値

        Returns
        -------
        PicoData
        """
        # convert ADC counts data to mV
        # ADC値をmVに変換
        adc2mv_ch1 = adc2mV(
            self._buffer_ch1_max,
            self._ch1_range,
            self._c_max_ADC
        )

        # Create time data
        # 時間軸のデータを生成
        time_data = np.linspace(
            0,
            self._c_sampled_num.value * self._c_time_interval_ns.value,
            self._c_sampled_num.value
        )

        picodata = PicoData(
            data=np.asarray([adc2mv_ch1]),
            time=time_data,
            set_values=self.check_set_values()
        )

        return picodata
