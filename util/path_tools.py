import pickle
from functools import partial
from pathlib import Path
from typing import Sequence, List, Union

import joblib


class PathTools(object):
    """
    指定したディレクトリもしくはファイルが存在するか確認して、そのPathオブジェクトを生成
    """

    def __init__(self):
        pass

    @staticmethod
    def create_dir(dir_path: str) -> Path:
        """
        指定したパスにディレクトリを生成する。

        Parameters
        ----------
        dir_path : str
            生成するディレクトリパス（Noneと指定した場合はカレントディレクトリになる）

        Returns
        -------
        pathlib.Paht
            Pathオブジェクトを生成
        """
        assert dir_path, "Need to specify the directory path. It is currently empty."

        p_dir = Path(dir_path) if dir_path else Path.cwd()

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir.mkdir(exist_ok=True, parents=True)

        return p_dir

    @staticmethod
    def path_obj(path: str) -> Path:
        """
        指定されたpathの存在確認などをせずに、とにかくPathオブジェクトを生成

        Parameters
        ----------
        path : str

        Returns
        -------
        pathlib.Paht
            Pathオブジェクトを生成
        """
        assert path, "Need to specify the path. It is currently empty."
        # Pathオブジェクトを生成
        p = Path(path)
        return p

    @staticmethod
    def checked_dir_path_obj(dir_path: str) -> Path:
        """
        対象とするディレクトリパスが存在していない場合はAssertionErrorとなる。

        Parameters
        ----------
        dir_path : str
            解析対象とするディレクトリの存在を確認し、存在する場合はPathオブジェクトを生成
        Returns
        -------
        pathlib.Paht
            Pathオブジェクトを生成
        """
        assert dir_path, "Need to specify the directory path. It is currently empty."

        # Pathオブジェクトを生成
        p_dir = Path(dir_path)

        assert p_dir.is_dir(), "The directory '{0}' does not exist!".format(str(p_dir))

        return p_dir

    @staticmethod
    def checked_file_path_obj(file_path: str) -> Path:
        """
        対象とするファイルパスが存在していない場合はAssertionErrorとなる。

        Parameters
        ----------
        file_path : str
            解析対象とするファイルの存在を確認し、存在する場合はPathオブジェクトを生成
        Returns
        -------
        pathlib.Paht
            Pathオブジェクトを生成
        """
        assert file_path, "Need to specify the file path. It is currently empty."

        # Pathオブジェクトを生成
        p_file = Path(file_path)

        assert p_file.is_file(), "The file '{0}' does not exist!".format(str(p_file))

        return p_file

    @staticmethod
    def paths_sort(paths: Sequence[Path]):
        #         return sorted(paths, key = lambda x: int(x.name))
        return sorted(paths, key=lambda x: x.name)


class GetFilesParentNameList(object):
    """
    __call__の引数としてファイルのパスを与え、そのパイルパスの存在を確認する。
    ファイルが存在する場合は、親のディレクトリのディレクトリ名をリストで返す。
    リストのインデックスの0番側が直上の親ディレクトリ名
    """

    def __init__(self):
        self._path_tools = PathTools()

    def __call__(self, input_file_path: str) -> Sequence[str]:
        """
        Parameters
        ----------
        input_file_path : str
            ファイルパスを指定
        Returns
        -------
        Sequence[str]
           親のディレクトリのディレクトリ名をリストで返す。
            リストのインデックスの0番側が直上の親ディレクトリ名
        """
        # 与えられたファイルパスのpathオブジェクト
        p_file = self._path_tools.checked_file_path_obj(input_file_path)
        return [str(p.name) for p in p_file.parents]


class GetFileListBySuffix(object):
    """
    __init__で指定した拡張子ファイルのファイル名をList[str]で返す
    解析対象のディレクトリは__call__時に文字列で指定
    """

    def __init__(self, file_suffix: Union[str, Sequence[str]], recursive: bool = False):
        """
        Parameters
        ----------
        file_suffix : Union[str, Sequence[str]]
            検索対象とするファイルの拡張子('.'付きの文字列)。複数の場合はリストに並べて渡す。
        recursive : bool
            再帰的に探索するかどうか
        """
        assert file_suffix, "Need to specify the file extension. It is currently empty."
        self._path_tools = PathTools()
        self._recursive = recursive

        self._file_suffix_list = []
        if type(file_suffix) is str:
            self._file_suffix_list.append(file_suffix)
        if type(file_suffix) is list:
            self._file_suffix_list = file_suffix

    def __call__(self, src_dir: str = None) -> List[str]:
        """
        Parameters
        ----------
        src_dir : str
            解析対象とするディレクトリパスを指定(指定しない場合はカレントディレクトリになる)
        Returns
        -------
        List[str]
            __init__で指定した拡張子ファイルのファイル名のリスト
        """
        # ディレクトリの有無のチェックとPathオブジェクトの生成
        # src_dir=Noneの場合はカレントディレクトリのpathオブジェクトを返す
        p_src_dir = self._path_tools.checked_dir_path_obj(src_dir) if src_dir else Path.cwd()

        file_names = []

        for suffix in self._file_suffix_list:
            wild_card = "*" + suffix
            if self._recursive:  # 再帰的に探索する場合
                wild_card = "**/" + wild_card

            # glob() で拡張子指定して抽出
            p_file = [p for p in p_src_dir.glob(wild_card)]

            # ファイル名をソートしておく（不要かも？）
            p_file = self._path_tools.paths_sort(p_file)

            # 絶対バスの文字列を取り出す
            p_file_str = [str(p.resolve()) for p in p_file]

            # データの追加
            file_names.extend(p_file_str)

        return file_names


class ZeroFillFileRename(GetFileListBySuffix):
    """
    __init__で指定した拡張子ファイルのファイル名を抽出し、ファイル名の数字の部分をゼロ埋めしてリネームする
    解析対象のディレクトリは__call__時に文字列で指定

    ファイル名を指定した文字で分割し、数値に置き換えることが可能な部分についてゼロ埋めしたファイル名に置き換える
    """

    def __init__(self, file_suffix: Union[str, Sequence[str]], split: str = '_', zfill_dim: int = 6):
        """
        Parameters
        ----------
        file_suffix : Union[str, Sequence[str]]
            検索対象とするファイルの拡張子('.'付きの文字列)。複数の場合はリストに並べて渡す
        split : str
            ファイル名を分割する際のsplit文字
        zfill_dim : int
            ゼロ埋めの桁数
        """
        self._path_tools = PathTools()
        super().__init__(file_suffix)
        self._zfill_dim = zfill_dim
        self._split = split

    def __call__(self, src_dir: str = None):
        """
        Parameters
        ----------
        src_dir : str
            解析対象とするディレクトリパスを指定
        Returns
        -------
        None
            対象となったファイルを実際にリネームするだけ
        """
        file_names = super().__call__(src_dir)

        p_file_names = [Path(f) for f in file_names]

        for p in p_file_names:
            file_path = p.resolve()
            new_name = self.zfill_name(file_path)
            print(new_name)
            p.rename(new_name)

    def zfill_name(self, file_path: str) -> str:
        """
        Parameters
        ----------
        file_path : str
            ファイルパス
        Returns
        -------
        str
            ゼロ埋めしたファイル名を返す
        """
        p_file = self._path_tools.checked_file_path_obj(file_path)
        stem = p_file.stem
        suffix = p_file.suffix
        split = stem.split(self._split)

        # 整数値に置き換えられる文字列の場合は一旦数値にしてから文字に戻す（ゼロ埋め文字をキャンセル）
        replaced = list(map(lambda x: str(int(x)) if x.isdecimal() else x, split))

        # 整数値に置き換えられる文字列の場合は、zfillでゼロ埋めした文字に変換する
        replaced = list(map(lambda x: x.zfill(self._zfill_dim) if x.isdecimal() else x, replaced))

        # ファイルのstem（ファイル名の拡張子を除外したもの
        r_stem = self._split.join(replaced)

        p_file = p_file.with_name(r_stem + suffix)

        return str(p_file.resolve())


class FilePathGenerator(object):
    """
    __call__の引数としてディレクトリのpathとファイル名を指定して、そのファイルの絶対pathをstrで生成する。
    その際、指定したディレクトリが存在しない場合は、そのディレクトリの生成も行う。

    このクラスは、保存したいファイルの格納先ディレクトリのチェックや自動生成のために用いる。
    """

    def __init__(self, output_dir: str = None):
        """
        Parameters
        ----------
        output_dir : str
            出力先のディレクトリ（指定しない場合はカレントディレクトリになる）
        """
        self._path_tools = PathTools()  # 2連アンダースコアだと子クラスから直接アクセスできない
        self._output_dir = output_dir

    def __call__(self, output_file_name: str, output_dir=None) -> str:
        """
        Parameters
        ----------
        output_file_name : str
            生成したいファイルパスのファイル名を指定
        output_dir
            出力先のディレクトリパス（指定しない場合は__init__で指定されたディレクトリ）

        Returns
        -------
        str
            生成したファイルパス
            （ファイルパスが格納される予定のディレクトリは自動生成される）
        """
        # 出力予定のファイル名を拡張して指定する必要がある
        assert output_file_name, "Need to specify the output_file_name. It is currently empty."

        # 引数が指定されている場合は、その引数をディレクトリパスとする
        out_dir = output_dir if output_dir else self._output_dir

        # ディレクトリのパスオブジェクト生成（ディレクトリが存在しない場合は生成も行う）
        p_out_dir = self._path_tools.create_dir(out_dir)

        # 出力するファイルのパスオブジェクトを生成
        '''(絶対pathが分かっているディレクトリにjoinしたファイル名であれば、
        存在しないファイルでもresolve()できる)'''
        p_out_file = p_out_dir.joinpath(output_file_name)

        # strで絶対ファイルパスを返す
        return str(p_out_file.resolve())


class ReferencedFilePathGenerator(FilePathGenerator):
    """
    __call__の引数として、生成するファイルパスの元になるファイルのファイルパスを指定、
    元のファイルの拡張子を変更した出力ファイルのファイルおよび出力パスを生成する

    FilePathGenerator()が生成するファイル名を渡すのに対し、このクラスは参照する
    ファイルのパスを文字列で渡して、それを元に元ファイルの拡張子を変更したファイルパスを生成する

    - call()の引数としてファイルのパスを与え、そのパイルパスの存在を確認する。
    - ファイルが存在する場合は、そのパイルパスに対応する出力予定のファイルパス
      （元ファイルの拡張子を変更したもの）を指定された拡張子で生成し、その絶対パスを文字列で返す。
    - 出力先のディレクトリが存在しない場合はディレクトリの生成も行う。
    """

    def __init__(self, gen_suffix: str, output_dir: str = None, add_name: str = None):
        """
        Parameters
        ----------
        gen_suffix : str
            生成するのファイルパスの拡張子を指定
        output_dir : str
            出力先のディレクトリ（指定しない場合は参照元ファイルのディレクトリと同じ場所）
        add_name : str
            生成するファイル名に文字列を追加したい場合に使用
        """
        assert gen_suffix, 'Provide suffix of generate file (out_suffix)!'
        super().__init__(output_dir)
        self._generate_suffix = gen_suffix
        self._add_name = add_name

    def __call__(self, org_file_path: str, output_dir=None, add_name: str = None) -> str:
        """
        Parameters
        ----------
        org_file_path : str
            生成するファイルパスの元になるファイルのファイルパスを指定
            (実在するファイルのパスじゃないとAssertionError)
        output_dir : str
            出力先のディレクトリ（指定しない場合は__init__で指定されたディレクトリ）
        add_name : str
            生成するファイル名に文字列を追加したい場合に使用

        Returns
        -------
        str
            生成したファイルパス
            （ファイルパスが格納される予定のディレクトリは自動生成される）
        """
        # ファイル名やパスを参照するための元ファイルを指定する必要がある
        assert org_file_path, "Need to specify the org_file_path. It is currently empty."

        # 与えられたファイルパスのpathオブジェクト
        p_file = self._path_tools.checked_file_path_obj(org_file_path)

        # 引数が指定されている場合は、その引数をディレクトリパスとする
        out_dir = output_dir if output_dir else self._output_dir

        # ディレクトリのパスオブジェクト生成（ディレクトリが存在しない場合は生成も行う）
        p_out_dir = self._path_tools.create_dir(out_dir) if out_dir else p_file.parent

        # 出力ファイル名を指定
        # 元ファイル名の拡張子を指定した拡張子に変えたもの
        p_gen_file = p_file.with_suffix(self._generate_suffix)

        # 名前の追加
        add_name = (self._add_name or '') + (add_name or '')  # None の場合も文字列として扱いたい
        if add_name:
            p_gen_file = p_gen_file.with_name(p_gen_file.stem + add_name + p_gen_file.suffix)

        if p_file == p_gen_file:  # ファイルパスが一致してしまう場合
            # 元のファイル名の先頭に'_'を追加した名前にする
            p_gen_file = p_gen_file.with_name('_' + p_gen_file.name)

        # 出力の絶対ファイルパスをstrで取得
        p_gen_file = p_out_dir.joinpath(p_gen_file.name)

        return str(p_gen_file.resolve())


#########################
''' ファイルの保存 '''


class FileSave(object):
    """
    保存するファイルのファイル名を指定してオブジェクトを生成（この時点ではNoneでもよい）
    保存するファイル名のsuffixは '.pickle' か '.joblib'でなければならない
    __call__で実行する際に保存するデータを与える
    """

    def __init__(self, save_file_path: str = None):
        self._method = {
            '.pickle': pickle.dump,
            '.joblib': partial(joblib.dump, compress=3),
        }
        self._save_file_path = save_file_path

    def suffix_check(self, file_path: str):
        suffix = Path(file_path).suffix
        assert suffix, 'Provide suffix of output file!'
        assert suffix in self._method.keys(), "Suffix must be '.pickle' or '.joblib'. But present value is {0}".format(
            suffix)
        return suffix

    def __call__(self, data, save_file_path: str = None):
        """
        dataで渡したデータを、save_file_pathで指定したファイル名（path）で保存する

        Parameters
        ----------
        data : 型未定
            保存するデータ
        save_file_path : str
            保存するデータのファイルパス
        Returns
        -------

        """
        if save_file_path:
            file_path = save_file_path
        else:
            file_path = self._save_file_path

        assert file_path, "Provide output filename with '.pickle' or '.joblib' suffix!"

        with open(file_path, mode='wb') as f:
            dump_method = self._method.get(self.suffix_check(file_path))
            dump_method(data, f)
